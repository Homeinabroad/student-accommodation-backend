from math import radians, sin, cos, sqrt, asin

from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.utils.text import slugify


class Currency(models.Model):
    id = models.AutoField(primary_key=True)
    currency_code = models.CharField(max_length=500)
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    added_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'currency_code'
        verbose_name = "Currency"
        verbose_name_plural = "Currencies"


class Country(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500)
    slug = models.SlugField(max_length=500, null=True, blank=True, editable=False)
    country_code = models.CharField(max_length=500)
    currency_code = models.ForeignKey(Currency, on_delete=models.CASCADE, null=True, blank=True)
    currency_symbol = models.CharField(max_length=500)
    banner_image = models.ImageField(upload_to="countries/banners/", null=True)
    logo = models.ImageField(upload_to="countries/logos/", null=True)
    thumbnail = models.ImageField(upload_to="countries/thumbnails/", null=True)
    description = models.TextField()
    ranking = models.PositiveIntegerField()
    is_featured = models.BooleanField()
    heading1 = models.CharField(max_length=100)
    heading2 = models.CharField(max_length=100)
    meta_title = models.CharField(max_length=500)
    meta_keyword = models.TextField()
    meta_description = models.TextField()
    added_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    is_enabled = models.BooleanField(default=True)
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
            country = Country.objects.filter(slug=self.slug).count()
            if country is True:
                self.slug = slugify("{} {}".format(self.name, country + 1))
        super(Country, self).save(*args, **kwargs)

    class Meta:
        db_table = 'country'
        verbose_name = "Country"
        verbose_name_plural = "Countries"


class City(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500)
    slug = models.SlugField(max_length=500, null=True, blank=True, editable=False)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    description = models.TextField()
    banner_image = models.ImageField(upload_to="cities/banners/", null=True)
    logo = models.ImageField(upload_to="cities/logos/", null=True)
    thumbnail = models.ImageField(upload_to="cities/thumbnails/", null=True)
    is_featured = models.BooleanField()
    ranking = models.PositiveIntegerField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    postal_code = models.CharField(max_length=10, null=True, blank=False)
    heading1 = models.CharField(max_length=100)
    heading2 = models.CharField(max_length=100)
    meta_title = models.CharField(max_length=500)
    meta_keyword = models.TextField()
    meta_description = models.TextField()
    added_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    status = models.IntegerField(default=1)
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    is_enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def country_slug(self):
        return self.country.slug

    def country_name(self):
        return self.country.name

    def currency_code(self):
        return self.country.currency_code.currency_code

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
            city = City.objects.filter(slug=self.slug).count()
            if city is True:
                self.slug = slugify("{} {}".format(self.name, city + 1))
        super(City, self).save(*args, **kwargs)

    class Meta:
        db_table = 'city'
        verbose_name = "City"
        verbose_name_plural = "Cities"


class Provider(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500)
    cities = models.ManyToManyField(City, blank=True)

    class Meta:
        db_table = 'provider'
        verbose_name = "Provider"
        verbose_name_plural = "Providers"


class University(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500)
    slug = models.SlugField(max_length=500, null=True, blank=True, editable=False)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    description = models.TextField()
    banner_image = models.ImageField(upload_to="universities/banners/", null=True)
    logo = models.ImageField(upload_to="universities/logos/", null=True)
    thumbnail = models.ImageField(upload_to="universities/thumbnails/", null=True)
    has_campus = models.BooleanField(max_length=500)
    campus_detail = models.TextField()
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    added_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    is_enabled = models.BooleanField(default=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
    heading1 = models.CharField(max_length=100)
    heading2 = models.CharField(max_length=100)
    meta_title = models.CharField(max_length=500)
    meta_keyword = models.TextField()
    meta_description = models.TextField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
            university = University.objects.filter(slug=self.slug).count()
            if university is True:
                self.slug = slugify("{} {}".format(self.name, university + 1))
        super(University, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.slug

    class Meta:
        db_table = 'university'
        verbose_name = "University"
        verbose_name_plural = "Universities"


class UniversityCampus(models.Model):
    name = models.CharField(max_length=100)
    university = models.ForeignKey(University, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=150, blank=True, null=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
    is_main = models.NullBooleanField(default=None)

    def save(self, *args, **kwargs):
        if not self.is_main:
            self.is_main = None
        self.slug = slugify(self.university.slug + "-" + slugify(self.name))
        super(UniversityCampus, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "University Campus"
        verbose_name_plural = "University Campuses"
        unique_together = [("university", "is_main")]


class PropertyFacility(models.Model):
    id = models.AutoField(primary_key=True)
    slug = models.SlugField(max_length=400, null=True, blank=True, editable=False)
    title = models.TextField()
    logo = models.ImageField(upload_to="facilities/", null=True)
    added_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)
            facility = PropertyFacility.objects.filter(slug=self.slug).count()
            if facility is True:
                self.facility_slug = slugify("{} {}".format(self.title, facility + 1))
        super(PropertyFacility, self).save(*args, **kwargs)

    class Meta:
        db_table = 'property_facility'
        verbose_name = "Property facility"
        verbose_name_plural = "Property facilities"


class Property(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=400)
    slug = models.SlugField(max_length=200, null=True, blank=True, editable=False)
    description = models.TextField()
    distance_from_city_center = models.FloatField(default=0)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    latitude = models.FloatField()
    longitude = models.FloatField()
    address = models.TextField()
    is_featured = models.BooleanField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    numbers_of_beds = models.IntegerField()
    ranking = models.IntegerField()
    property_type = models.CharField(max_length=500)
    rating = models.FloatField()
    base_currency_code = models.CharField(max_length=500)
    why_book = models.TextField()
    payment_procedure = models.TextField()
    price_unit = models.CharField(max_length=50, blank=False, null=True)
    is_enabled = models.BooleanField(default=True)
    is_property_extra_ordinary = models.BooleanField(default=True)
    listing_order = models.IntegerField()
    nearby_universities = models.ManyToManyField(University, blank=True)
    amenities = models.ManyToManyField(PropertyFacility, blank=True, related_name="amenities")
    rent_inclusions = models.ManyToManyField(PropertyFacility, blank=True, related_name="rent_inclusions")
    safety_inclusions = models.ManyToManyField(PropertyFacility, blank=True, related_name="safety_inclusions")
    heading1 = models.CharField(max_length=100)
    heading2 = models.CharField(max_length=100)
    meta_title = models.TextField()
    meta_keywords = models.TextField()
    meta_description = models.TextField()
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    added_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def city_slug(self):
        return str(self.city.slug)

    def city_name(self):
        return self.city.name

    def country(self):
        return self.city.country.id

    def country_name(self):
        return self.city.country_name()

    def country_slug(self):
        return str(self.city.country.slug)

    def images(self):
        return self.property_images.all()

    def first_image(self):
        obj = self.property_images.first()
        if obj:
            return obj.image.url
        return None

    def propertyOffers(self):
        return self.property_offers.all()

    def roomCategories(self):
        return self.room_categories.all()

    def universities(self):
        return self.nearby_universities.all()

    def min_bed_pricing(self):
        categories = self.room_categories.values_list('id', flat=True)
        room_types = RoomType.objects.filter(room_category__id__in=categories).values_list('id', flat=True)
        bed_pricings = BedPricing.objects.filter(room_type__id__in=room_types).values_list('bed_price', flat=True)
        if bed_pricings:
            return min(bed_pricings)
        else:
            return 0

    def distance_from_point(self, latitude, longitude):
        return Property.get_distance(latitude1=latitude, longitude1=longitude,
                                     latitude2=self.latitude, longitude2=self.longitude)

    @staticmethod
    def get_distance(latitude1, longitude1, latitude2, longitude2):
        lon1 = radians(longitude1)
        lon2 = radians(longitude2)
        lat1 = radians(latitude1)
        lat2 = radians(latitude2)

        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2

        c = 2 * asin(sqrt(a))

        # Radius of earth in kilometers. Use 3956 for miles
        r = 6371

        return c * r

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
            property = Property.objects.filter(slug=self.slug).count()
            if property is True:
                self.slug = slugify("{} {}".format(self.name, property + 1))
        super(Property, self).save(*args, **kwargs)

    class Meta:
        db_table = 'property'
        verbose_name = "Property"
        verbose_name_plural = "Properties"


class PropertyImage(models.Model):
    property = models.ForeignKey(Property, on_delete=models.CASCADE, related_name="property_images", blank=False,
                                 null=True)
    image = models.ImageField(upload_to="properties/", null=False)

    class Meta:
        verbose_name = "Property Image"
        verbose_name_plural = "Property Images"


class PropertyOffers(models.Model):
    id = models.AutoField(primary_key=True)
    property = models.ForeignKey(Property, on_delete=models.CASCADE, related_name='property_offers',
                                 null=True, blank=True)
    title = models.CharField(max_length=500)
    message = models.TextField()
    validity_date = models.DateField()
    is_enabled = models.BooleanField(default=True)

    class Meta:
        db_table = 'property_offer'
        verbose_name = "Property Offer"
        verbose_name_plural = "Property Offers"


class RoomCategory(models.Model):
    id = models.AutoField(primary_key=True)
    property = models.ForeignKey(Property, on_delete=models.CASCADE, related_name='room_categories',
                                 null=True, blank=False)
    name = models.CharField(max_length=400)
    description = models.TextField()
    banner_image = models.ImageField(upload_to="room_categories/banners", null=True)
    updated_date = models.DateTimeField(auto_now=True)
    added_date = models.DateTimeField(auto_now_add=True)
    is_enabled = models.BooleanField(default=True)

    def roomTypes(self):
        return self.room_types.all()

    def images(self):
        return self.category_images.all()

    def categoryOffers(self):
        return self.category_offers.all()

    class Meta:
        db_table = 'room_categories'
        verbose_name = "Room Category"
        verbose_name_plural = "Room Categories"


class RoomCategoryImage(models.Model):
    room_category = models.ForeignKey(RoomCategory, on_delete=models.CASCADE, blank=False,
                                      related_name="category_images", null=True)
    image = models.ImageField(upload_to="room_categories/", null=False)

    class Meta:
        verbose_name = "Room Category Image"
        verbose_name_plural = "Room Category Images"


class RoomCategoryOffer(models.Model):
    id = models.AutoField(primary_key=True)
    room_category = models.ForeignKey(RoomCategory, on_delete=models.CASCADE, related_name='category_offers',
                                      null=True, blank=False)
    title = models.CharField(max_length=500)
    message = models.TextField()
    validity_date = models.DateField()
    added_date = models.DateTimeField(auto_now_add=True)
    is_enabled = models.BooleanField(default=True)

    class Meta:
        db_table = 'room_categories_offer'
        verbose_name = "Room Category Offer"
        verbose_name_plural = "Room Category Offers"


class RoomType(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=500)
    area = models.TextField()
    room_category = models.ForeignKey(RoomCategory, on_delete=models.CASCADE, related_name='room_types',
                                      null=True, blank=False)
    facilities = models.ManyToManyField(PropertyFacility, blank=True)
    added_date = models.DateTimeField(auto_now_add=True)
    is_enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    def typeOffers(self):
        return self.room_type_offers.all()

    def bedPricing(self):
        return self.room_type_bed_pricing.all()

    def images(self):
        return self.room_type_images.all()

    def property(self):
        return self.room_category.property.name

    class Meta:
        db_table = 'room_type'
        verbose_name = "Room Type"
        verbose_name_plural = "Room Types"


class RoomTypeImage(models.Model):
    room_type = models.ForeignKey(RoomType, on_delete=models.CASCADE, blank=False, related_name="room_type_images",
                                  null=True)
    image = models.ImageField(upload_to="room_types", null=False)

    class Meta:
        verbose_name = "Room Type Image"
        verbose_name_plural = "Room Type Images"


class RoomTypeOffer(models.Model):
    id = models.AutoField(primary_key=True)
    room_type = models.ForeignKey(RoomType, on_delete=models.CASCADE, related_name='room_type_offers',
                                  null=True, blank=False)
    title = models.CharField(max_length=500)
    message = models.TextField()
    validity_date = models.DateField()
    added_date = models.DateTimeField(auto_now_add=True)
    is_enabled = models.BooleanField(default=True)

    class Meta:
        db_table = 'roomtype_offer'
        verbose_name = "Room Type Offers"
        verbose_name_plural = "Room Type Offers"


class BedPricing(models.Model):
    id = models.AutoField(primary_key=True)
    room_type = models.ForeignKey(RoomType, on_delete=models.CASCADE, related_name='room_type_bed_pricing',
                                  null=True, blank=False)
    is_short_term = models.BooleanField(default=True)
    has_nominated_beds = models.BooleanField(default=True)
    number_of_nominated_beds = models.IntegerField(default=0)
    bed_availability_status = models.TextField()
    rent_breakup_duration = models.CharField(max_length=400)
    tenancy_duration = models.CharField(max_length=400)
    tenancy_duration_unit = models.CharField(max_length=400)
    room_checkin_date = models.DateField()
    room_checkout_date = models.DateField()
    bed_price = models.IntegerField(default=0)
    bed_discounted_price = models.IntegerField()
    deposit_charge = models.IntegerField(default=0)
    service_fee = models.IntegerField(default=0)
    taxes_inclusive = models.IntegerField(default=0)
    display_price_bed = models.TextField()
    display_discount_price_bed = models.TextField()

    class Meta:
        db_table = 'bed_price'
        verbose_name = "Bed Pricing"
        verbose_name_plural = "Bed Pricings"


class ContactUs(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500)
    email = models.EmailField()
    contact_no = models.CharField(max_length=500)
    message = models.TextField()
    lead_source = models.TextField()
    added_date = models.DateTimeField(auto_now_add=True)
    is_enabled = models.BooleanField(default=True)

    class Meta:
        db_table = 'contact_us'
        verbose_name = "Contact Us"
        verbose_name_plural = "Contact Us"


class WebUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="web_user")
    gender = models.CharField(max_length=200, null=True)
    contact_number = models.CharField(max_length=200)
    dob = models.DateField(null=True)
    nationality = models.CharField(max_length=200, null=True)
    university = models.ForeignKey(University, on_delete=models.CASCADE, blank=True, null=True)
    year_of_study = models.CharField(max_length=4, null=True)

    class Meta:
        verbose_name = "Web User"
        verbose_name_plural = "Web Users"

    def __str__(self):
        return self.first_name()

    def email(self):
        return self.user.email

    def first_name(self):
        return self.user.first_name

    def last_name(self):
        return self.user.last_name

    def bookings(self):
        return self.user_bookings.all()

    def password(self):
        return self.user.password

    def delete(self, using=None, keep_parents=False):
        user = self.user
        user.delete()
        super(WebUser, self).delete(using=using, keep_parents=keep_parents)


class Booking(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(WebUser, blank=False, on_delete=models.CASCADE, related_name="user_bookings")
    lead_source = models.TextField()
    property = models.ForeignKey(Property, on_delete=models.CASCADE)
    room_type = models.ForeignKey(RoomType, on_delete=models.CASCADE)
    bed_price = models.ForeignKey(BedPricing, on_delete=models.CASCADE)
    check_in_date = models.DateField()
    checkout_date = models.DateField()
    added_date = models.DateTimeField(auto_now=True)
    is_enabled = models.BooleanField(default=True)

    def property_slug(self):
        return self.property.slug

    def property_image(self):
        return self.property.first_image()

    def email(self):
        return self.user.user.email

    def first_name(self):
        return self.user.user.first_name

    def last_name(self):
        return self.user.user.last_name

    def contact_number(self):
        return self.user.contact_number

    def gender(self):
        return self.user.gender

    def dob(self):
        return self.user.dob

    def year_of_study(self):
        return self.user.year_of_study

    def nationality(self):
        return self.user.nationality

    def university(self):
        return self.user.university

    class Meta:
        db_table = 'booking'
        verbose_name = "Booking"
        verbose_name_plural = "Bookings"


class Enquiry(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=500)
    last_name = models.CharField(max_length=500)
    email = models.EmailField()
    contact_number = models.CharField(max_length=200)
    university = models.ForeignKey(University, on_delete=models.CASCADE)
    lead_source = models.TextField()
    message = models.TextField()
    property = models.ForeignKey(Property, on_delete=models.CASCADE)
    room_category = models.ForeignKey(RoomCategory, on_delete=models.CASCADE)
    room_type = models.ForeignKey(RoomType, on_delete=models.CASCADE)
    bed_price = models.ForeignKey(BedPricing, on_delete=models.CASCADE)
    check_in_date = models.DateField()
    checkout_date = models.DateField()
    is_enabled = models.BooleanField(default=True)

    class Meta:
        db_table = 'enquiry'
        verbose_name = "Enquiry"
        verbose_name_plural = "Enquiries"


class LeadGeneration(models.Model):
    first_name = models.CharField(max_length=400)
    last_name = models.CharField(max_length=400)
    email = models.EmailField()
    contact_number = models.CharField(max_length=400)
    university = models.CharField(max_length=100, blank=False, null=True)
    lead_source = models.CharField(max_length=400)
    message = models.TextField()
    budget = models.TextField(null=True, blank=True)
    room_type = models.CharField(max_length=100, blank=False, null=True)

    class Meta:
        db_table = 'lead_generation'
        verbose_name = "Lead Generation"
        verbose_name_plural = "Lead generations"


class CMSUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="cms_user")
    contact_number = models.CharField(max_length=13, blank=False)

    class Meta:
        verbose_name = "CMS User"
        verbose_name_plural = "CMS Users"

    def __str__(self):
        return self.email()

    def first_name(self):
        return self.user.first_name

    def last_name(self):
        return self.user.last_name

    def email(self):
        return self.user.email

    def password(self):
        return self.user.password

    def delete(self, using=None, keep_parents=False):
        user = self.user
        user.delete()
        super(CMSUser, self).delete(using=using, keep_parents=keep_parents)


class NewsLetterSubscription(models.Model):
    email = models.EmailField(blank=False, unique=True)
    subscribed_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "News Letter Subscription"
        verbose_name_plural = "News Letter Subscriptions"


class ServiceRequest(models.Model):
    first_name = models.CharField(max_length=100, null=False, blank=False)
    last_name = models.CharField(max_length=100, null=False, blank=False)
    email = models.EmailField(null=False, blank=False)
    contact_number = models.CharField(max_length=15, null=False, blank=False)
    message = models.TextField(max_length=1000, null=False, blank=False)
    service_name = models.CharField(max_length=100, null=False, blank=False)
    added_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.service_name}<{self.email}>"

    class Meta:
        verbose_name = "Service Request"
        verbose_name_plural = "Service Requests"


class Referral(models.Model):
    referrer_first_name = models.CharField(max_length=100, blank=False, null=False)
    referrer_last_name = models.CharField(max_length=100, blank=False, null=False)
    referrer_email = models.EmailField(null=False)
    referrer_contact_number = models.CharField(max_length=13, blank=False, null=False)
    message = models.TextField(max_length=1000, blank=False, null=False)
    friend_first_name = models.CharField(max_length=100, blank=False, null=False)
    friend_last_name = models.CharField(max_length=100, blank=False, null=False)
    friend_email = models.EmailField(null=False)
    friend_contact_number = models.CharField(max_length=13, blank=False, null=False)
    friend_city_of_study = models.ForeignKey(City, on_delete=models.SET_NULL, null=True)
    friend_university = models.ForeignKey(University, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "Referral" + str(self.id)

    class Meta:
        verbose_name = "Referral"
        verbose_name_plural = "Referrals"
