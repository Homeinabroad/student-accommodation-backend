from django.apps import AppConfig


class StudentAccomodationConfig(AppConfig):
    name = 'student_accomodation'

    def ready(self):
        import student_accomodation.signals
