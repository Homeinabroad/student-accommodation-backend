import django.dispatch
from django.core.mail import send_mail
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django_rest_passwordreset.signals import reset_password_token_created
from student_api.settings import FROM_EMAIL


email_signal = django.dispatch.Signal(providing_args=["data"])


@receiver(email_signal)
def save_user_profile(sender, data, **kwargs):
    if data:
        sender.send_email(data=data)


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    data = {
        "first_name": reset_password_token.user.first_name,
        "last_name": reset_password_token.user.last_name,
        "token": reset_password_token.key,
        "email": reset_password_token.user.email
    }

    subject = "Password Reset Link"
    html_message = render_to_string('student_accomodation/emails/forgetPassword.html', context=data)
    plain_message = strip_tags(html_message)

    send_mail(subject, plain_message, FROM_EMAIL, [data["email"]], html_message=html_message,
              fail_silently=True)
