from rest_framework import serializers
from student_accomodation.models import City, Country


class CitySerializer(serializers.ModelSerializer):
    added_by = serializers.StringRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = City
        fields = ['id', 'name', 'slug', 'country', 'country_slug', 'country_name', 'description', 'banner_image',
                  'logo', 'thumbnail', 'is_featured', 'ranking', 'latitude', 'longitude', 'postal_code', 'heading1',
                  'heading2', 'meta_title', 'meta_keyword', 'meta_description', 'added_date', 'updated_date', 'status',
                  'is_enabled', 'added_by']


class CityReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ["id", "name", "slug", "country_name", "country_slug", "banner_image", "description", "currency_code"]


class CountrySerializer(serializers.ModelSerializer):
    added_by = serializers.StringRelatedField(default=serializers.CurrentUserDefault(), read_only=True)

    class Meta:
        model = Country
        fields = "__all__"
