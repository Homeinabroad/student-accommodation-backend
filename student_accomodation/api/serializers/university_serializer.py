from rest_framework import serializers

from student_accomodation.api.serializers.city_serializers import CityReadSerializer
from student_accomodation.models import University


class UniversitySerializer(serializers.ModelSerializer):
    added_by = serializers.StringRelatedField(default=serializers.CurrentUserDefault(), read_only=True)

    class Meta:
        model = University
        fields = "__all__"


class UniversityReadSerializer(serializers.ModelSerializer):
    city = CityReadSerializer()

    class Meta:
        model = University
        fields = ["id", "name", "slug", "city", "latitude", "longitude", "campus_detail"]


class UniversityListSerializer(serializers.ModelSerializer):
    city = CityReadSerializer()

    class Meta:
        model = University
        fields = "__all__"
