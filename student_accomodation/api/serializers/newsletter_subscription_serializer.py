from rest_framework import serializers
from student_accomodation.models import NewsLetterSubscription


class NewsLetterSubscriptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsLetterSubscription
        fields = "__all__"
