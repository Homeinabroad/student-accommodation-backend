from rest_framework import serializers
from student_accomodation.models import LeadGeneration


class LeadGenerationSerializer(serializers.ModelSerializer):

    class Meta:
        model = LeadGeneration
        fields = "__all__"
