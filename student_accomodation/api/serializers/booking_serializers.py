from rest_framework import serializers

from student_accomodation.api.serializers.bed_pricing_serializer import BedPricingShortSerializer
from student_accomodation.api.serializers.room_type_serializer import RoomTypeReadSerializer, RoomTypeShortSerializer
from student_accomodation.models import Booking, Property, University


class BookingSerializer(serializers.ModelSerializer):
    property = serializers.PrimaryKeyRelatedField(queryset=Property.objects.all(), required=True)
    university = serializers.PrimaryKeyRelatedField(queryset=University.objects.all(), required=True)
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    contact_number = serializers.CharField()
    email = serializers.CharField()
    gender = serializers.CharField()
    dob = serializers.CharField()
    year_of_study = serializers.CharField()
    nationality = serializers.CharField()

    def to_internal_value(self, data):
        internal_value = super(BookingSerializer, self).to_internal_value(data=data)
        if "email" in internal_value:
            internal_value.pop("email")
        if "first_name" in internal_value:
            internal_value.pop("first_name")
        if "last_name" in internal_value:
            internal_value.pop("last_name")
        if "gender" in internal_value:
            internal_value.pop("gender")
        if "year_of_study" in internal_value:
            internal_value.pop("year_of_study")
        if "dob" in internal_value:
            internal_value.pop("dob")
        if "contact_number" in internal_value:
            internal_value.pop("contact_number")
        if "nationality" in internal_value:
            internal_value.pop("nationality")
        if "university" in internal_value:
            internal_value.pop("university")
        return internal_value

    class Meta:
        model = Booking
        fields = ["id", "first_name", "last_name", "email", "gender", "contact_number", "dob", "year_of_study",
                  "nationality", "university", "property", "lead_source", "room_type", "bed_price",
                  "check_in_date", "checkout_date"]


class BookingReadSerializer(serializers.ModelSerializer):
    property = serializers.StringRelatedField(read_only=True)
    university = serializers.StringRelatedField(read_only=True)
    lead_source = serializers.StringRelatedField(read_only=True)
    room_type = RoomTypeShortSerializer(read_only=True)
    bed_price = BedPricingShortSerializer(read_only=True)

    class Meta:
        model = Booking
        fields = ["id", "first_name", "last_name", "email", "gender", "contact_number", "dob", "year_of_study",
                  "nationality", "university", "property", "property_image", "lead_source", "room_type", "bed_price",
                  "check_in_date", "checkout_date"]
