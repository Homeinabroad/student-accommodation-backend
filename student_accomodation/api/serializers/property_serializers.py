from rest_framework import serializers

from student_accomodation.api.serializers.property_facility_serializer import PropertyFacilityReadSerializer
from student_accomodation.api.serializers.room_category_serializer import PropertyRoomCategorySerializer, \
    PropertyRoomCategoryReadSerializer
from student_accomodation.api.serializers.university_serializer import UniversityReadSerializer
from student_accomodation.models import *


class PropertyImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = PropertyImage
        fields = "__all__"


class PropertyOfferSerializer(serializers.ModelSerializer):

    class Meta:
        model = PropertyOffers
        fields = "__all__"


class PropertySerializer(serializers.ModelSerializer):
    propertyOffers = PropertyOfferSerializer(many=True, required=False)
    roomCategories = PropertyRoomCategorySerializer(many=True, required=False)
    images = PropertyImageSerializer(many=True, required=False)
    universities = serializers.PrimaryKeyRelatedField(many=True, queryset=University.objects.all(), required=False)
    amenities = serializers.PrimaryKeyRelatedField(many=True, queryset=PropertyFacility.objects.all(), required=False)
    rent_inclusions = serializers.PrimaryKeyRelatedField(many=True, queryset=PropertyFacility.objects.all(),
                                                         required=False)
    safety_inclusions = serializers.PrimaryKeyRelatedField(many=True, queryset=PropertyFacility.objects.all(),
                                                           required=False)
    added_by = serializers.StringRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Property
        fields = ('id', 'name', 'slug', 'description', 'distance_from_city_center', 'provider', 'latitude',
                  'longitude', 'address', 'is_featured', 'country', "country_name", 'country_slug', 'city', 'city_slug',
                  "city_name", 'numbers_of_beds', 'ranking', 'property_type', 'rating', 'base_currency_code', 'why_book'
                  , 'payment_procedure', 'payment_procedure', 'price_unit', 'is_enabled', 'is_property_extra_ordinary'
                  , 'min_bed_pricing', 'listing_order', 'heading1', 'heading2', 'meta_title'
                  , 'meta_keywords', 'meta_description', 'added_date', 'updated_date', 'images'
                  , 'propertyOffers', 'roomCategories', 'universities', 'amenities', 'rent_inclusions'
                  , 'safety_inclusions', 'added_by')

    def create(self, validated_data):
        images_validated_data = validated_data.pop("images") if "images" in validated_data else []
        offers_validated_data = validated_data.pop('propertyOffers') if "propertyOffers" in validated_data else []
        amenities = validated_data.pop('amenities') if "amenities" in validated_data else []
        rent_inclusions = validated_data.pop('rent_inclusions') if "rent_inclusions" in validated_data else []
        safety_inclusions = validated_data.pop('safety_inclusions') if "safety_inclusions" in validated_data else []
        room_categories_validated_data = validated_data.pop('roomCategories') if "roomCategories" in validated_data \
            else []
        universities = validated_data.pop('universities') if "universities" in validated_data else []

        validated_data['slug'] = slugify(validated_data['name'])
        new_property = Property.objects.create(**validated_data)

        if offers_validated_data:
            offers_serializer = self.fields["propertyOffers"]
            for offer in offers_validated_data:
                offer['property'] = new_property
            offers_serializer.create(offers_validated_data)

        if images_validated_data:
            image_serializer = self.fields["images"]
            for image in images_validated_data:
                image["property"] = new_property
            image_serializer.create(images_validated_data)

        if amenities:
            for facility in amenities:
                new_property.amenities.add(facility)

        if rent_inclusions:
            for facility in rent_inclusions:
                new_property.rent_inclusions.add(facility)

        if safety_inclusions:
            for facility in safety_inclusions:
                new_property.safety_inclusions.add(facility)

        if universities:
            for university_id in universities:
                new_property.nearby_universities.add(university_id)

        if room_categories_validated_data:
            room_categories_serializer = self.fields["roomCategories"]
            for room_category in room_categories_validated_data:
                room_category['property'] = new_property
            room_categories_serializer.create(room_categories_validated_data)

        return new_property

    def update(self, instance, validated_data):
        university_data = validated_data.pop("universities")
        amenities_data = validated_data.pop("amenities")
        rent_inclusion_data = validated_data.pop("rent_inclusions")
        safety_inclusion_data = validated_data.pop("safety_inclusions")
        instance = super(PropertySerializer, self).update(instance=instance, validated_data=validated_data)

        for university in university_data:
            instance.nearby_universities.add(university)

        for amenity in amenities_data:
            instance.amenities.add(amenity)

        for rent_inclusion in rent_inclusion_data:
            instance.rent_inclusions.add(rent_inclusion)

        for safety_inclusion in safety_inclusion_data:
            instance.safety_inclusions.add(safety_inclusion)

        return instance


class PropertyReadSerializer(serializers.ModelSerializer):
    universities = UniversityReadSerializer(many=True, read_only=True)
    amenities = PropertyFacilityReadSerializer(many=True, read_only=True)
    rent_inclusions = PropertyFacilityReadSerializer(many=True, read_only=True)
    safety_inclusions = PropertyFacilityReadSerializer(many=True, read_only=True)
    propertyOffers = PropertyOfferSerializer(many=True, required=False)
    roomCategories = PropertyRoomCategoryReadSerializer(many=True, required=False)
    images = PropertyImageSerializer(many=True, required=False)
    added_by = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Property
        fields = ('id', 'name', 'slug', 'description', 'distance_from_city_center', 'provider', 'latitude',
                  'longitude', 'address', 'is_featured', 'country', 'country_name', 'country_slug', 'city', 'city_name'
                  , 'city_slug', 'numbers_of_beds', 'ranking', 'property_type', 'rating', 'base_currency_code'
                  , 'why_book', 'payment_procedure', 'payment_procedure', 'price_unit', 'is_enabled',
                  'is_property_extra_ordinary', 'min_bed_pricing', 'listing_order', 'heading1', 'heading2', 'meta_title'
                  , 'meta_keywords', 'meta_description', 'added_date', 'updated_date', 'images'
                  , 'propertyOffers', 'roomCategories', 'universities', 'amenities', 'rent_inclusions'
                  , 'safety_inclusions', 'added_by')


class PropertyShortSerializer(serializers.ModelSerializer):

    class Meta:
        model = Property
        fields = ["name", "slug", "country", "country_name", "country_slug", "city", "city_name", "city_slug",
                  "min_bed_pricing", 'rating', 'price_unit', 'first_image']


class PropertyUniversitySerializer(serializers.ModelSerializer):
    distance = serializers.FloatField(read_only=True, default=0)
    amenities = PropertyFacilityReadSerializer(many=True, read_only=True)
    propertyOffers = PropertyOfferSerializer(many=True, required=False)

    class Meta:
        model = Property
        fields = ["name", "slug", "country", "country_name", "country_slug", "city", "city_name", "city_slug",
                  "min_bed_pricing", 'rating', 'price_unit', 'first_image', "distance", "amenities", "propertyOffers"]
