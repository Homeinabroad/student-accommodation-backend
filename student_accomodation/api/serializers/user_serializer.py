from django.contrib.auth.models import User
from rest_framework.exceptions import ValidationError

from student_accomodation.api.serializers.booking_serializers import BookingReadSerializer
from student_accomodation.models import WebUser, University, CMSUser
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class MainUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['first_name', "last_name", "username", "email", "password", "id"]

    def create(self, validated_data):
        password = validated_data.pop("password")
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    bookings = BookingReadSerializer(many=True, required=False, read_only=True)
    password = serializers.CharField(required=True)
    university = serializers.PrimaryKeyRelatedField(queryset=University.objects.all(), required=False)

    class Meta:
        model = WebUser
        fields = ["id", "email", "first_name", "last_name", "contact_number", "gender", "dob", "nationality",
                  "year_of_study", "university", "bookings", "password"]
        
    def validate_email(self, value):
        if User.objects.filter(email=value):
            raise ValidationError(detail="User with the email id already exists.", code=400)
        return value
        
    def to_internal_value(self, data):
        internal_value = super(UserSerializer, self).to_internal_value(data=data)
        internal_value.update({
            "password": data.get("password", "")
        })
        return internal_value

    def create(self, validated_data):
        first_name = validated_data.pop("first_name")
        last_name = validated_data.pop("last_name")
        password = validated_data.pop("password")
        email = validated_data.pop("email")

        credentials = {
            "first_name": first_name,
            "last_name": last_name,
            "password": password,
            "email": email,
            "username": email
        }
        user_serializer = MainUserSerializer()
        user = user_serializer.create(validated_data=credentials)
        try:
            new_user = WebUser.objects.create(**validated_data, user=user)
        except Exception as e:
            user.delete()
            return {"message": f"Error: {e}"}
        return new_user


class CMSUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    class Meta:
        model = CMSUser
        fields = ['first_name', 'last_name', 'email', 'contact_number', 'password']

    def to_internal_value(self, data):
        internal_value = super(CMSUserSerializer, self).to_internal_value(data=data)
        internal_value.update({
            "password": data.get("password", "")
        })
        return internal_value

    def create(self, validated_data):
        first_name = validated_data.pop("first_name")
        last_name = validated_data.pop("last_name")
        password = validated_data.pop("password")
        email = validated_data.pop("email")

        credentials = {
            "first_name": first_name,
            "last_name": last_name,
            "password": password,
            "email": email,
            "username": email
        }
        user_serializer = MainUserSerializer()
        user = user_serializer.create(validated_data=credentials)
        new_user = CMSUser.objects.create(**validated_data, user=user)
        return new_user


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def update(self, instance, validated_data):
        return super(MyTokenObtainPairSerializer, self).update(instance=instance, validated_data=validated_data)

    def create(self, validated_data):
        return super(MyTokenObtainPairSerializer, self).create(validated_data=validated_data)

    def validate(self, attr):
        data = super().validate(attr)
        token = self.get_token(self.user)
        data['user'] = self.user.id
        return data


class ChangePasswordSerializer(serializers.Serializer):
    model = User

    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
