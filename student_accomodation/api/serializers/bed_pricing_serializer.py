from rest_framework import serializers

from student_accomodation.models import BedPricing


class BedPricingSerializer(serializers.ModelSerializer):

    class Meta:
        model = BedPricing
        fields = "__all__"


class BedPricingShortSerializer(serializers.ModelSerializer):

    class Meta:
        model = BedPricing
        fields = ["bed_price"]
