from rest_framework import serializers
from student_accomodation.models import Referral, City, University


class ReferralSerializer(serializers.ModelSerializer):
    friend_city_of_study = serializers.SlugRelatedField(queryset=City.objects.all(), slug_field="slug")
    friend_university = serializers.SlugRelatedField(queryset=University.objects.all(), slug_field="slug")

    class Meta:
        model = Referral
        fields = "__all__"
