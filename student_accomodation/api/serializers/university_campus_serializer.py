from rest_framework import serializers
from student_accomodation.models import UniversityCampus


class UniversityCampusSerializer(serializers.ModelSerializer):

    class Meta:
        model = UniversityCampus
        fields = "__all__"

