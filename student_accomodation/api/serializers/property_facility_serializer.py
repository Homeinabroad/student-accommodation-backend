from rest_framework import serializers
from student_accomodation.models import PropertyFacility


class PropertyFacilitySerializer(serializers.ModelSerializer):
    added_by = serializers.StringRelatedField(default=serializers.CurrentUserDefault(), read_only=True)

    class Meta:
        model = PropertyFacility
        fields = "__all__"


class PropertyFacilityReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = PropertyFacility
        fields = ["id", "title", "slug", "logo"]
