from rest_framework import serializers

from student_accomodation.api.serializers.bed_pricing_serializer import BedPricingSerializer
from student_accomodation.api.serializers.property_facility_serializer import PropertyFacilityReadSerializer
from student_accomodation.models import RoomTypeImage, RoomType, RoomTypeOffer, PropertyFacility


class RoomTypeOfferSerializer(serializers.ModelSerializer):

    class Meta:
        model = RoomTypeOffer
        fields = "__all__"


class RoomTypeImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = RoomTypeImage
        fields = "__all__"


class PropertyRoomTypeSerializer(serializers.ModelSerializer):
    images = RoomTypeImageSerializer(many=True, required=False)
    typeOffers = RoomTypeOfferSerializer(many=True, required=False)
    bedPricing = BedPricingSerializer(many=True, required=False)
    facilities = serializers.PrimaryKeyRelatedField(many=True, queryset=PropertyFacility.objects.all(), required=False)

    class Meta:
        model = RoomType
        fields = ['id', 'title', "area", 'room_category', 'added_date', 'is_enabled', 'images',
                  'typeOffers', 'bedPricing', 'facilities']

    def to_internal_value(self, data):
        internal_value = super(PropertyRoomTypeSerializer, self).to_internal_value(data=data)
        internal_value.update({
            "typeOffers": data["typeOffers"] if "typeOffers" in data else [],
            "bedPricing": data["bedPricing"] if "bedPricing" in data else [],
            "images": data["images"] if "images" in data else []
        })
        return internal_value

    def create(self, validated_data):
        images_validated_data = validated_data.pop("images") if "images" in validated_data else []
        offers_validated_data = validated_data.pop('typeOffers') if "typeOffers" in validated_data else []
        bed_pricing_validated_data = validated_data.pop('bedPricing') if "bedPricing" in validated_data else []
        facilities = validated_data.pop('facilities') if "facilities" in validated_data else []

        new_room_type = RoomType.objects.create(**validated_data)

        if offers_validated_data:
            offers_type_serializer = self.fields["typeOffers"]
            for offer in offers_validated_data:
                offer['room_type'] = new_room_type
            offers_type_serializer.create(offers_validated_data)

        if images_validated_data:
            image_serializer = self.fields["images"]
            for image in images_validated_data:
                image["room_type"] = new_room_type
            image_serializer.create(images_validated_data)

        if bed_pricing_validated_data:
            bed_pricing_serializer = self.fields["bedPricing"]
            for pricing in bed_pricing_validated_data:
                pricing['room_type'] = new_room_type
            bed_pricing_serializer.create(bed_pricing_validated_data)

        if facilities:
            for facility in facilities:
                new_room_type.facilities.add(facility)

        return new_room_type


class RoomTypeReadSerializer(serializers.ModelSerializer):
    images = RoomTypeImageSerializer(many=True, read_only=True)
    typeOffers = RoomTypeOfferSerializer(many=True, read_only=True)
    bedPricing = BedPricingSerializer(many=True, read_only=True)
    facilities = PropertyFacilityReadSerializer(many=True, read_only=True)

    class Meta:
        model = RoomType
        fields = ['id', 'title', "area", 'room_category', 'added_date', 'is_enabled', 'images',
                  'typeOffers', 'bedPricing', 'facilities']


class RoomTypeShortSerializer(serializers.ModelSerializer):
    images = RoomTypeImageSerializer(many=True, read_only=True)

    class Meta:
        model = RoomType
        fields = ['id', 'title', 'area', 'images', 'property', 'images']
