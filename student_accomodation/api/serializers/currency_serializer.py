from rest_framework import serializers
from student_accomodation.models import Currency


class CurrencySerializer(serializers.ModelSerializer):
    added_by = serializers.StringRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Currency
        fields = "__all__"
