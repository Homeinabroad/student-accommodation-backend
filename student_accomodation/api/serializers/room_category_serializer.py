from rest_framework import serializers

from student_accomodation.api.serializers.room_type_serializer import PropertyRoomTypeSerializer, RoomTypeReadSerializer
from student_accomodation.models import RoomCategoryOffer, RoomCategory, RoomCategoryImage


class RoomCategoryOfferSerializer(serializers.ModelSerializer):

    class Meta:
        model = RoomCategoryOffer
        fields = "__all__"


class RoomCategoryImageSerializer(serializers.ModelSerializer):
    room_category = serializers.PrimaryKeyRelatedField(required=False, queryset=RoomCategory.objects.all())

    class Meta:
        model = RoomCategoryImage
        fields = "__all__"


class PropertyRoomCategorySerializer(serializers.ModelSerializer):
    images = RoomCategoryImageSerializer(many=True, required=False)
    roomTypes = PropertyRoomTypeSerializer(many=True, required=False)
    categoryOffers = RoomCategoryOfferSerializer(many=True, required=False)

    class Meta:
        model = RoomCategory
        fields = ["id", "property", "name", "description", "banner_image",
                  "updated_date", "added_date", "is_enabled", "images", "roomTypes", "categoryOffers"]

    def create(self, validated_data):
        images_validated_data = validated_data.pop("images") if "images" in validated_data else []
        room_types_validated_data = validated_data.pop('roomTypes') if "roomTypes" in validated_data else []
        offers_validated_data = validated_data.pop('categoryOffers') if "categoryOffers" in validated_data else []

        new_room_category = RoomCategory.objects.create(**validated_data)

        if room_types_validated_data:
            room_type_serializer = self.fields["roomTypes"]
            for room_type in room_types_validated_data:
                room_type['room_category'] = new_room_category
            room_type_serializer.create(room_types_validated_data)

        if images_validated_data:
            image_serializer = self.fields["images"]
            for image in images_validated_data:
                image["room_category"] = new_room_category
            image_serializer.create(images_validated_data)

        if offers_validated_data:
            category_offer_serializer = self.fields["categoryOffers"]
            for offer in offers_validated_data:
                offer['room_category'] = new_room_category
            category_offer_serializer.create(offers_validated_data)

        return new_room_category


class PropertyRoomCategoryReadSerializer(serializers.ModelSerializer):
    images = RoomCategoryImageSerializer(many=True, required=False)
    roomTypes = RoomTypeReadSerializer(many=True, required=False)
    categoryOffers = RoomCategoryOfferSerializer(many=True, required=False)

    class Meta:
        model = RoomCategory
        fields = ["id", "property", "name", "description", "banner_image",
                  "updated_date", "added_date", "is_enabled", "images", "roomTypes", "categoryOffers"]
