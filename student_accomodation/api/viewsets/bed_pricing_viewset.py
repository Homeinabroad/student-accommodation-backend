from rest_framework import viewsets

from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, get_permissions_by_action
from student_accomodation.api.serializers.bed_pricing_serializer import BedPricingSerializer
from student_accomodation.models import BedPricing


class BedPricingViewSet(viewsets.ModelViewSet):
    serializer_class = BedPricingSerializer
    queryset = BedPricing.objects.all()
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)
