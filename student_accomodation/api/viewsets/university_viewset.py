from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from student_accomodation.api.serializers.property_serializers import PropertyUniversitySerializer
from student_accomodation.api.serializers.university_campus_serializer import UniversityCampusSerializer
from student_accomodation.api.serializers.university_serializer import UniversitySerializer, UniversityListSerializer, \
    UniversityReadSerializer
from student_accomodation.models import University, UniversityCampus, Property
from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, get_permissions_by_action


class UniversityViewSet(viewsets.ModelViewSet):
    serializer_class = UniversitySerializer
    queryset = University.objects.all()
    filterset_fields = ["city", "city__slug"]
    permissions = DEFAULT_PERMISSIONS
    permissions.update({'nearby_properties': [], 'search': []})
    permission_classes_by_action = permissions

    def get_permissions(self):
        return get_permissions_by_action(self)

    def get_serializer_class(self):
        if self.action == "list" or self.action == "retrieve":
            return UniversityListSerializer
        return UniversitySerializer

    def perform_create(self, serializer):
        req = serializer.context['request']
        serializer.save(added_by=req.user)


class UniversitySlugViewSet(UniversityViewSet):
    lookup_field = "slug"
    http_method_names = ["get"]

    @action(methods=["get"], detail=True, name="properties", url_name="properties", url_path="properties",
            description="Gives all properties near the university sorted by distance.")
    def nearby_properties(self, request, slug=None):
        university = self.get_object()
        latitude, longitude = university.latitude, university.longitude
        if not latitude and not longitude:
            try:
                campus = UniversityCampus.objects.get(university=university, is_main=True)
                latitude, longitude = campus.latitude, campus.longitude
            except UniversityCampus.DoesNotExist:
                pass
        properties = Property.objects.filter(nearby_universities=university)
        distances = {}
        for property in properties:
            distances[property.slug] = property.distance_from_point(latitude=latitude, longitude=longitude)
        property_serializer = PropertyUniversitySerializer(instance=properties, many=True)
        data = property_serializer.data
        for property in data:
            property["distance"] = distances[property["slug"]]
        data = sorted(data, key=lambda property: property["distance"])
        return Response(data=data, status=status.HTTP_200_OK)

    @action(methods=["get"], url_path="search/(?P<search_term>[0-9a-zA-Z]+)", detail=False)
    def search(self, request, search_term):
        universities = University.objects.filter(name__search=search_term)
        serializer = UniversityReadSerializer(instance=universities, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class UniversityCampusViewset(viewsets.ModelViewSet):
    queryset = UniversityCampus.objects.all()
    serializer_class = UniversityCampusSerializer
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)
