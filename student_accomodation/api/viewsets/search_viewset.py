from rest_framework.response import Response
from rest_framework.views import APIView

from student_accomodation.api.serializers.property_serializers import *
from student_accomodation.api.serializers.university_serializer import *
from student_accomodation.api.serializers.user_serializer import *


class SearchTerm(APIView):
    """
    Search Cities, Universities, Properties
    """

    @staticmethod
    def get(request, search_term):
        cities = City.objects.all()
        properties = Property.objects.all()
        universities = University.objects.all()
        if search_term:
            cities = City.objects.filter(name__icontains=search_term)
            properties = Property.objects.filter(name__icontains=search_term)
            universities = University.objects.filter(name__icontains=search_term)
        city_serializer = CityReadSerializer(cities, many=True)
        property_serializer = PropertyShortSerializer(properties, many=True)
        university_serializer = UniversityReadSerializer(universities, many=True)
        return Response({"cities": city_serializer.data,
                         "properties": property_serializer.data,
                         "universities": university_serializer.data
                         })
