from rest_framework import viewsets

from student_accomodation.api.serializers.city_serializers import CountrySerializer
from student_accomodation.models import Country
from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, get_permissions_by_action


class CountryViewSet(viewsets.ModelViewSet):
    serializer_class = CountrySerializer
    queryset = Country.objects.all()
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)

    def perform_create(self, serializer):
        req = serializer.context['request']
        serializer.save(added_by=req.user)
