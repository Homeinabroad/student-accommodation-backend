from rest_framework import viewsets

from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, get_permissions_by_action
from student_accomodation.api.serializers.currency_serializer import CurrencySerializer
from student_accomodation.models import Currency


class CurrencyViewSet(viewsets.ModelViewSet):
    serializer_class = CurrencySerializer
    queryset = Currency.objects.all()
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)
