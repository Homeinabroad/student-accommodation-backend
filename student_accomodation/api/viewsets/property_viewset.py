from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, get_permissions_by_action
from student_accomodation.api.serializers.property_facility_serializer import PropertyFacilitySerializer
from student_accomodation.api.serializers.property_serializers import PropertyReadSerializer, PropertySerializer, \
    PropertyShortSerializer, PropertyImageSerializer, PropertyOfferSerializer
from student_accomodation.models import Property, PropertyImage, PropertyFacility, PropertyOffers


class PropertyViewSet(viewsets.ModelViewSet):
    queryset = Property.objects.all().order_by('listing_order')
    permission_classes = (IsAuthenticated, )
    filterset_fields = ["slug", "city__slug"]
    lookup_field = 'id'
    http_method_names = ["post", "put", "patch", "delete"]
    permissions = DEFAULT_PERMISSIONS.copy()
    permissions.update({'featured': []})
    permission_classes_by_action = permissions

    def get_permissions(self):
        return get_permissions_by_action(self)

    def get_serializer_class(self):
        if self.action == "list" or self.action == "retrieve":
            return PropertyReadSerializer
        return PropertySerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        read_serializer = PropertyReadSerializer(instance=instance)
        return Response(read_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        req = serializer.context['request']
        instance = serializer.save(added_by=req.user)
        return instance


class PropertySlugViewSet(PropertyViewSet):
    lookup_field = 'slug'
    http_method_names = ['get']

    @action(methods=["get"], detail=False, url_path="featured", url_name="featured",
            description="Returns featured properties sorted by ranking.")
    def featured(self, request):
        """
        Returns featured properties sorted by ranking.
        """
        instance = Property.objects.filter(is_featured=True).order_by('ranking')
        serializer = PropertyShortSerializer(instance=instance, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class PropertyImageViewSet(viewsets.ModelViewSet):
    serializer_class = PropertyImageSerializer
    queryset = PropertyImage.objects.all()
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)


class PropertyFacilityViewSet(viewsets.ModelViewSet):
    serializer_class = PropertyFacilitySerializer
    queryset = PropertyFacility.objects.all()
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)

    def perform_create(self, serializer):
        req = serializer.context['request']
        serializer.save(added_by=req.user)


class PropertyOfferViewSet(viewsets.ModelViewSet):
    serializer_class = PropertyOfferSerializer
    queryset = PropertyOffers.objects.all()
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)
