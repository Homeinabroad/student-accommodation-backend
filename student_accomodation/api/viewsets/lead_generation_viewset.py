import datetime

from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from rest_framework import viewsets

from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, IsCMSUser, get_permissions_by_action
from student_accomodation.api.serializers.lead_generation_serializers import LeadGenerationSerializer
from student_accomodation.models import LeadGeneration
from student_accomodation.signals import email_signal
from student_api.settings import FROM_EMAIL


class LeadGenerationViewSet(viewsets.ModelViewSet):
    serializer_class = LeadGenerationSerializer
    queryset = LeadGeneration.objects.all()
    permissions = DEFAULT_PERMISSIONS.copy()
    permissions.update({'create': [], 'list': [IsCMSUser], 'retrieve': [IsCMSUser]})
    permission_classes_by_action = permissions

    def get_permissions(self):
        return get_permissions_by_action(self)

    def create(self, request, *args, **kwargs):
        response = super(LeadGenerationViewSet, self).create(request=request, *args, **kwargs)
        data = response.data
        email_signal.send(sender=self.__class__, data=data)
        return response

    @staticmethod
    def send_email(data):
        subject1 = f"Someone contacted You - {datetime.datetime.now()}"
        subject2 = f"Thank You for Contacting Us."

        html_message_1 = render_to_string('student_accomodation/emails/AdminContactUs.html', context=data)
        plain_message_1 = strip_tags(html_message_1)

        html_message_2 = render_to_string('student_accomodation/emails/Contact.html')
        plain_message_2 = strip_tags(html_message_2)

        send_mail(subject2, plain_message_2, FROM_EMAIL, [data["email"]], html_message=html_message_2,
                  fail_silently=True)
        send_mail(subject1, plain_message_1, FROM_EMAIL, [FROM_EMAIL], html_message=html_message_1,
                  fail_silently=True)
