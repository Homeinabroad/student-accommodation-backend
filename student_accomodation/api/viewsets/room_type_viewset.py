from rest_framework import viewsets, status
from rest_framework.response import Response

from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, get_permissions_by_action
from student_accomodation.api.serializers.room_type_serializer import PropertyRoomTypeSerializer, \
    RoomTypeReadSerializer, RoomTypeImageSerializer, RoomTypeOfferSerializer
from student_accomodation.models import RoomType, RoomTypeImage, RoomTypeOffer


class PropertyRoomTypeViewSet(viewsets.ModelViewSet):
    serializer_class = PropertyRoomTypeSerializer
    queryset = RoomType.objects.all()
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)

    def get_serializer_class(self):
        if self.action == "list" or self.action == "retrieve":
            return RoomTypeReadSerializer
        return PropertyRoomTypeSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        read_serializer = RoomTypeReadSerializer(instance=instance)
        return Response(read_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        instance = serializer.save()
        return instance


class RoomTypeImageViewSet(viewsets.ModelViewSet):
    serializer_class = RoomTypeImageSerializer
    queryset = RoomTypeImage.objects.all()
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)


class RoomTypeOfferViewSet(viewsets.ModelViewSet):
    serializer_class = RoomTypeOfferSerializer
    queryset = RoomTypeOffer.objects.all()
    permission_classes_by_action = DEFAULT_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)
