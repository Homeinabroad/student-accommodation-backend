import datetime

from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from student_accomodation.api.permissions import BOOKING_PERMISSIONS, get_permissions_by_action
from student_accomodation.api.reset_password import get_password_reset_token
from student_accomodation.api.serializers.booking_serializers import BookingSerializer, BookingReadSerializer
from student_accomodation.api.serializers.user_serializer import UserSerializer
from student_accomodation.models import Booking, WebUser
from student_accomodation.signals import email_signal
from student_api.settings import FROM_EMAIL


class BookingViewSet(viewsets.ModelViewSet):
    serializer_class = BookingSerializer
    queryset = Booking.objects.all()
    permission_classes_by_action = BOOKING_PERMISSIONS

    def get_permissions(self):
        return get_permissions_by_action(self)

    def get_serializer_class(self):
        if self.action == "list" or self.action == "retrieve":
            return BookingReadSerializer
        return BookingSerializer

    def get_queryset(self):
        if self.action == "list" or self.action == "retrieve":
            return Booking.objects.filter(user__user=self.request.user)
        return Booking.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        req = serializer.context["request"]
        instance = self.create_booking(serializer, user=WebUser.objects.get(user=req.user))
        headers = self.get_success_headers(serializer.data)
        read_serializer = BookingReadSerializer(instance=instance)
        data = read_serializer.data
        signal_data = {
            'tenancy': f"{instance.bed_price.tenancy_duration} {instance.bed_price.tenancy_duration}",
            'room_category': f"{instance.room_type.room_category}",
            'is_guest': False
        }
        signal_data.update(data)
        email_signal.send(sender=self.__class__, data=signal_data)
        return Response(data, status=status.HTTP_201_CREATED, headers=headers)

    @action(methods=["post"], detail=False, url_path="guest", url_name="guest_booking")
    def guest_booking(self, request):
        data = request.data
        users = WebUser.objects.filter(user__email=data["email"])
        if users:
            user = users[0]
            is_guest = False
        else:
            user = BookingViewSet.create_guest_user(data=data)
            is_guest = True
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            instance = self.create_booking(serializer, user)
        except KeyError as e:
            return Response({f"{e}": "Field required."}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(e.args[0], status=status.HTTP_400_BAD_REQUEST)
        headers = self.get_success_headers(serializer.data)
        read_serializer = BookingReadSerializer(instance=instance)
        data = read_serializer.data
        signal_data = {
            'tenancy': f"{instance.bed_price.tenancy_duration} {instance.bed_price.tenancy_duration}",
            'room_category': f"{instance.room_type.room_category}",
            'is_guest': is_guest
        }
        signal_data.update(data)
        email_signal.send(sender=self.__class__, data=signal_data)
        return Response(data, status=status.HTTP_201_CREATED, headers=headers)

    @staticmethod
    def create_booking(serializer, user):
        instance = serializer.save(user=user)
        return instance

    @staticmethod
    def create_guest_user(data):
        try:
            user_data = {
                "email": data["email"],
                "contact_number": data["contact_number"],
                "dob": data["dob"],
                "first_name": data["first_name"],
                "last_name": data["last_name"],
                "password": "YouAreInAbroad123",
                "gender": data["gender"],
                "nationality": data["nationality"],
                "year_of_study": data["year_of_study"],
                "university": data["university"]
            }
        except KeyError as e:
            raise ValidationError(detail=f"{e.__traceback__} field required.", code=400)
        user_serializer = UserSerializer(data=user_data)
        user_serializer.is_valid(raise_exception=True)
        instance = user_serializer.save()
        return instance

    @staticmethod
    def send_email(data):

        subject1 = f"Someone created booking request - {datetime.datetime.now()}"
        subject2 = f"Thank You for choosing HomeinAbroad for your student accommodation search"
        data["booking_date"] = datetime.datetime.now()
        if data["is_guest"]:
            data["token"] = get_password_reset_token(data=data).key

        html_message_1 = render_to_string('student_accomodation/emails/adminBook.html', context=data)
        plain_message_1 = strip_tags(html_message_1)

        html_message_2 = render_to_string('student_accomodation/emails/Booking.html', context=data)
        plain_message_2 = strip_tags(html_message_2)

        send_mail(subject2, plain_message_2, FROM_EMAIL, [data["email"]], html_message=html_message_2,
                  fail_silently=True)
        send_mail(subject1, plain_message_1, FROM_EMAIL, [FROM_EMAIL], html_message=html_message_1,
                  fail_silently=True)
