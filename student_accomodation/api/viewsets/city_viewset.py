from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, get_permissions_by_action
from student_accomodation.api.serializers.city_serializers import CitySerializer, CityReadSerializer
from student_accomodation.models import City


class CityViewSet(viewsets.ModelViewSet):
    serializer_class = CitySerializer
    queryset = City.objects.all()
    permissions = DEFAULT_PERMISSIONS.copy()
    permissions.update({'featured': [], 'detail_list': []})
    permission_classes_by_action = permissions

    def get_permissions(self):
        return get_permissions_by_action(self)

    def perform_create(self, serializer):
        req = serializer.context['request']
        serializer.save(added_by=req.user)

    def get_serializer_class(self):
        if self.action == 'list':
            return CityReadSerializer
        return CitySerializer


class CitySlugViewSet(CityViewSet):
    lookup_field = 'slug'
    http_method_names = ["get"]

    @action(methods=["get"], detail=False, url_path="detail", url_name="detail",
            description="Returns detailed list of all cities.")
    def detail_list(self, request):
        """
        Returns detailed list of all cities.
        """
        serializer = CitySerializer(instance=City.objects.all(), many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(methods=["get"], url_path="featured", url_name="featured_cities", detail=False,
            description="Returns featured cities sorted by ranking.")
    def featured(self, request):
        """
        Returns featured cities sorted by ranking.
        """
        instance = City.objects.filter(is_featured=True).order_by("ranking")
        serializer = CityReadSerializer(instance=instance, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
