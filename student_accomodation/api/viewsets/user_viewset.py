from django.contrib.auth.models import User
from rest_framework import viewsets, status
from rest_framework.generics import UpdateAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView

from student_accomodation.api.serializers.user_serializer import UserSerializer, CMSUserSerializer, \
    MyTokenObtainPairSerializer, ChangePasswordSerializer
from student_accomodation.models import WebUser, CMSUser
from student_accomodation.api.permissions import USER_PERMISSIONS, get_permissions_by_action


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = WebUser.objects.all()

    permission_classes_by_action = USER_PERMISSIONS.copy()

    def get_permissions(self):
        return get_permissions_by_action(self)

    def get_queryset(self):
        if self.action in ["list", "retrieve", "update", "partial_update"] \
                and not any([hasattr(self.request.user, "cms_user"),
                             self.request.user.is_superuser,
                             self.request.user.is_staff]):
            return WebUser.objects.filter(user=self.request.user)
        return WebUser.objects.all()


class CMSUserViewSet(viewsets.ModelViewSet):
    serializer_class = CMSUserSerializer
    queryset = CMSUser.objects.all()
    permission_classes = (IsAdminUser, )


class LoginView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class ChangePasswordView(UpdateAPIView):
    """
    User should be logged in. Changes user password, not reset it.
    """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)
    http_method_names = ["put"]

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
