from rest_framework import viewsets

from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, IsCMSUser, get_permissions_by_action
from student_accomodation.models import Referral
from student_accomodation.api.serializers.referral_serializers import ReferralSerializer


class ReferralViewSet(viewsets.ModelViewSet):
    queryset = Referral.objects.all()
    serializer_class = ReferralSerializer
    permissions = DEFAULT_PERMISSIONS.copy()
    permissions.update({'create': [], 'list': [IsCMSUser], 'retrieve': [IsCMSUser]})
    permission_classes_by_action = permissions

    def get_permissions(self):
        return get_permissions_by_action(self)
