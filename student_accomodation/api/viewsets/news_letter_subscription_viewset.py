from rest_framework import viewsets

from student_accomodation.api.permissions import DEFAULT_PERMISSIONS, IsCMSUser, get_permissions_by_action
from student_accomodation.api.serializers.newsletter_subscription_serializer import NewsLetterSubscriptionSerializer
from student_accomodation.models import NewsLetterSubscription


class NewLetterSubscriptionViewSet(viewsets.ModelViewSet):
    serializer_class = NewsLetterSubscriptionSerializer
    queryset = NewsLetterSubscription.objects.all()
    permissions = DEFAULT_PERMISSIONS.copy()
    permissions.update({'create': [], 'list': [IsCMSUser], 'retrieve': [IsCMSUser]})
    permission_classes_by_action = permissions

    def get_permissions(self):
        return get_permissions_by_action(self)
