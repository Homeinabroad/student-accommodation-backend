from rest_framework.permissions import IsAuthenticated, IsAdminUser


class IsCMSUser(IsAuthenticated):
    def has_permission(self, request, view):
        return super(IsCMSUser, self).has_permission(request, view) and (hasattr(request.user, "cms_user")
                                                                         or request.user.is_superuser
                                                                         or request.user.is_staff)


def get_permissions_by_action(obj):
    try:
        return [permission() for permission in obj.permission_classes_by_action[obj.action]]
    except KeyError:
        return [permission() for permission in obj.permission_classes]


DEFAULT_PERMISSIONS = {'create': [IsCMSUser],
                       'list': [],
                       'retrieve': [],
                       'update': [IsCMSUser],
                       'partial_update': [IsCMSUser],
                       'destroy': [IsAdminUser]}

BOOKING_PERMISSIONS = {'create': [IsAuthenticated],
                       'list': [IsAuthenticated],
                       'retrieve': [IsAuthenticated],
                       'update': [IsCMSUser],
                       'partial_update': [IsCMSUser],
                       'guest_booking': [],
                       'destroy': [IsAdminUser]}

USER_PERMISSIONS = {'create': [],
                    'list': [IsAuthenticated],
                    'retrieve': [IsAuthenticated],
                    'update': [IsAuthenticated],
                    'partial_update': [IsAuthenticated],
                    'destroy': [IsAdminUser]}
