from django.contrib import admin
from .models import *


# Register your models here.
admin.site.register(Provider)
admin.site.register(City)
admin.site.register(Country)
admin.site.register(Currency)
admin.site.register(Property)
admin.site.register(PropertyOffers)
admin.site.register(University)
admin.site.register(RoomCategory)
admin.site.register(RoomCategoryOffer)
admin.site.register(RoomType)
admin.site.register(RoomTypeOffer)
admin.site.register(BedPricing)
admin.site.register(Booking)
admin.site.register(PropertyFacility)
admin.site.register(WebUser)
admin.site.register(CMSUser)
admin.site.register(PropertyImage)
admin.site.register(RoomCategoryImage)
admin.site.register(RoomTypeImage)
admin.site.register(LeadGeneration)
admin.site.register(NewsLetterSubscription)
admin.site.register(UniversityCampus)
