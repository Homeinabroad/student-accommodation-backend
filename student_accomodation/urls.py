from django.urls import path
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from rest_framework.documentation import include_docs_urls

from student_accomodation.api.viewsets.bed_pricing_viewset import BedPricingViewSet
from student_accomodation.api.viewsets.booking_viewset import BookingViewSet
from student_accomodation.api.viewsets.city_viewset import CityViewSet, CitySlugViewSet
from student_accomodation.api.viewsets.country_viewset import CountryViewSet
from student_accomodation.api.viewsets.currency_viewset import CurrencyViewSet
from student_accomodation.api.viewsets.enquiry_viewset import EnquiryViewSet
from student_accomodation.api.viewsets.lead_generation_viewset import LeadGenerationViewSet
from student_accomodation.api.viewsets.news_letter_subscription_viewset import NewLetterSubscriptionViewSet
from student_accomodation.api.viewsets.property_viewset import PropertyViewSet, PropertySlugViewSet, \
    PropertyOfferViewSet, PropertyFacilityViewSet, PropertyImageViewSet
from student_accomodation.api.viewsets.provider_viewset import ProviderViewSet
from student_accomodation.api.viewsets.referral_viewset import ReferralViewSet
from student_accomodation.api.viewsets.room_category_viewset import PropertyRoomCategoryViewSet, \
    RoomCategoryOfferViewSet, RoomCategoryImageViewSet
from student_accomodation.api.viewsets.room_type_viewset import PropertyRoomTypeViewSet, RoomTypeOfferViewSet, \
    RoomTypeImageViewSet
from student_accomodation.api.viewsets.search_viewset import SearchTerm
from student_accomodation.api.viewsets.service_request_viewset import ServiceRequestViewSet
from student_accomodation.api.viewsets.university_viewset import UniversityViewSet, UniversitySlugViewSet, \
    UniversityCampusViewset
from student_accomodation.api.viewsets.user_viewset import UserViewSet, CMSUserViewSet, LoginView, ChangePasswordView

router = routers.SimpleRouter()
router.register(r'cms/property', viewset=PropertyViewSet)
router.register(r'property', viewset=PropertySlugViewSet)
router.register(r'property_offers', viewset=PropertyOfferViewSet)
router.register(r'facilities', viewset=PropertyFacilityViewSet)
router.register(r'room_category', viewset=PropertyRoomCategoryViewSet)
router.register(r'room_type', viewset=PropertyRoomTypeViewSet)
router.register(r'bed_pricing', viewset=BedPricingViewSet)
router.register(r'category_offer', viewset=RoomCategoryOfferViewSet)
router.register(r'type_offer', viewset=RoomTypeOfferViewSet)
router.register(r'cms/university', viewset=UniversityViewSet)
router.register(r'university', viewset=UniversitySlugViewSet)
router.register(r'university_campus', viewset=UniversityCampusViewset)
router.register(r'cms/city', viewset=CityViewSet)
router.register(r'city', viewset=CitySlugViewSet)
router.register(r'country', viewset=CountryViewSet)
router.register(r'booking', viewset=BookingViewSet)
router.register(r'user', viewset=UserViewSet)
router.register(r'enquiry', viewset=EnquiryViewSet)
router.register(r'lead_generation', viewset=LeadGenerationViewSet)
router.register(r'provider', viewset=ProviderViewSet)
router.register(r'currency', viewset=CurrencyViewSet)
router.register(r'cms/user', viewset=CMSUserViewSet)
router.register(r'property_image', viewset=PropertyImageViewSet)
router.register(r'room_category_image', viewset=RoomCategoryImageViewSet)
router.register(r'room_type_image', viewset=RoomTypeImageViewSet)
router.register(r'newsletter_subscription', viewset=NewLetterSubscriptionViewSet)
router.register(r'service_request', viewset=ServiceRequestViewSet)
router.register(r'referral', viewset=ReferralViewSet)

urlpatterns = [
    path(r'docs/', include_docs_urls(title='HIA API', schema_url="")),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('search/<str:search_term>', SearchTerm.as_view(), name='token_refresh'),
    path('login/', LoginView.as_view(), name="Login"),
    path('password/change', ChangePasswordView.as_view(), name='change_password'),
]

urlpatterns += router.urls
